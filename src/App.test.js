import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders application logo', () => {
  const { getByAltText } = render(<App />);
  const logoImage = getByAltText(/logo/i);
  expect(logoImage).toBeInTheDocument();
});
