import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ModalContentImage = ({ alt, itemImageURL }) => {
  const [loading, setLoading] = useState(true);
  function imageLoaded(e) {
    setLoading(false);
  }
  return (
    <div className="content-image-wrapper">
      {loading ? (
        <FontAwesomeIcon className="icon" icon="spinner" spin />
      ) : undefined}
      <img
        className="modal-ingredient-image"
        src={itemImageURL}
        alt={alt}
        title={alt}
        onLoad={imageLoaded}
        style={loading ? { display: "none" } : {}}
      />
    </div>
  );
};

export default ModalContentImage;
