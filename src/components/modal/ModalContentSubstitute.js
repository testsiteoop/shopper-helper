import React from "react";

const ModalContentSubstitute = ({ substitutes }) => {
  return (
    <table className="substitutes">
      <tbody>
        {substitutes.map((substitute, index) => {
          return (
            <tr key={index}>
              <td>{substitute}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default ModalContentSubstitute;
