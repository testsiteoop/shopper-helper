import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ModalContentHelp = () => {
  return (
    <div>
      <p>
        Shopper Helper keeps a <b>list of ingredients</b> handy while you go
        grocery shopping. If you find one of them isn't available, try hitting
        the "<b>substitute</b>" button for possible alternatives!
      </p>
      <p>
        Check the source code:
        <br />
        <FontAwesomeIcon className="icon" icon={["fab", "bitbucket"]} />
        <br />
        <a href="https://bitbucket.org/testsiteoop/shopper-helper/">
          shopper-helper @ Bitbucket
        </a>
      </p>
      <p>
        Project | Andres Cruz García <br />
        <a href="https://demo.bloq.cl/">demo.bloq.cl</a>
      </p>
      <p className="attribution">
        "Grocery Bag" icon made by{" "}
        <a href="https://smashicons.com/" title="Smashicons">
          Smashicons
        </a>{" "}
        from{" "}
        <a href="https://www.flaticon.com/" title="Flaticon">
          {" "}
          www.flaticon.com
        </a>
      </p>
    </div>
  );
};

export default ModalContentHelp;
