import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Modal = ({ id, title, content, loading }) => {
  //hyphenate
  const titleAsId = title.replace(/\W+/g, "-").toLowerCase();
  if (loading) {
    title = "Loading...";
    content = <FontAwesomeIcon className="icon" icon="spinner" spin />;
  }
  return (
    <div className="modal micromodal-slide" id={id} aria-hidden="true">
      <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
        <div
          className="modal__container"
          role="dialog"
          aria-modal="true"
          aria-labelledby={`${id}-title`}
        >
          <header className="modal__header">
            <h2 className="modal__title" id={`${id}-title`}>
              {title}
            </h2>
          </header>
          <main className="modal__content" id={`${titleAsId}-content`}>
            {content}
          </main>
          <footer className="modal__footer">
            <button
              className="modal__btn"
              data-micromodal-close
              aria-label="Close this dialog window"
            >
              Close
            </button>
          </footer>
        </div>
      </div>
    </div>
  );
};

export default Modal;
