import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const EmptyListPlaceholder = () => {
  return (
    <div className={"empty-list-placeholder"}>
      <FontAwesomeIcon className="icon" icon={"arrow-circle-up"} size="6x"/>
      <p>
        Add some ingredients to your list!
      </p>
    </div>
  );
};

export default EmptyListPlaceholder;
