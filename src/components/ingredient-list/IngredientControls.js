import React from "react";

import IControl from "./IControl";

const IngredientControls = ({
  substituteIngredientHandler,
  deleteIngredientHandler,
  ingredientRef,
}) => {
  return (
    <div className="ingredient-controls animate__animated animate__slideInRight animate__faster">
      <IControl
        ingredientRef={ingredientRef}
        name="substitute"
        icon={"sync-alt"}
        text={"substitute"}
        onClickHandler={substituteIngredientHandler}
        modal
      />
      <IControl
        ingredientRef={ingredientRef}
        name="delete"
        icon={"times-circle"}
        text={"delete"}
        onClickHandler={deleteIngredientHandler}
      />
    </div>
  );
};

export default IngredientControls;
