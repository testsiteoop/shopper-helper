import React from "react";
import IngredientItem from "./IngredientItem";
import EmptyListPlaceholder from "./EmptyListPlaceholder";

const IngredientList = ({
  ingredients,
  modalIngrImageHandler,
  selectIngredientHandler,
  baseImageUrl,
  substituteIngredientHandler,
  deleteIngredientHandler,
}) => {
  const itemList = [];
  for (let [key, ingredient] of ingredients) {
    itemList.push(
      <IngredientItem
        key={`${key}`}
        itemName={ingredient.name}
        itemImageURL={`${baseImageUrl}${ingredient.image}`}
        isSelected={ingredient.isSelected}
        modalIngrImageHandler={modalIngrImageHandler}
        selectIngredientHandler={selectIngredientHandler}
        substituteIngredientHandler={substituteIngredientHandler}
        deleteIngredientHandler={deleteIngredientHandler}
      />
    );
  }
  return (
    <div
      className="ingredient-list-wrapper"
      name={"ingredient-list-wrapper"}
      onClick={selectIngredientHandler}
    >
      {itemList.length === 0 ? (
        <EmptyListPlaceholder />
      ) : (
        <ul className="ingredient-list">{itemList}</ul>
      )}
    </div>
  );
};

export default IngredientList;
