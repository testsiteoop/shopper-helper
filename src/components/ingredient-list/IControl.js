import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const IControl = ({
  className,
  name,
  icon,
  text,
  ingredientRef,
  onClickHandler,
  modal,
}) => {
  return (
    <div
      className={`i-control ${name} ${className ? className : ""}`}
      name={name}
      data-ingredient={ingredientRef}
      onClick={onClickHandler}
      data-micromodal-trigger={modal ? "modal" : undefined} // undefined prevents attribute
    >
      <div className="i-control-icon">
        <FontAwesomeIcon icon={icon} />
      </div>
      <span className="i-control-text">{text}</span>
    </div>
  );
};

export default IControl;
