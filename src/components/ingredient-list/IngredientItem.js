import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import IngredientControls from "./IngredientControls";

const IngredientItem = ({
  itemName,
  itemImageURL,
  isSelected,
  modalIngrImageHandler,
  selectIngredientHandler,
  substituteIngredientHandler,
  deleteIngredientHandler,
}) => {
  const [loading, setLoading] = useState(true);
  const imageLoaded = (e) => {
    setLoading(false);
  };
  return (
    <li
      className={`ingredient-item ${isSelected ? "selected" : ""}`}
      name={itemName}
      onClick={selectIngredientHandler}
    >
      <figure
        className="ingredient-image"
        onClick={modalIngrImageHandler}
        name={itemName}
        data-micromodal-trigger="modal"
      >
        {loading ? (
          <FontAwesomeIcon className="icon" icon="spinner" spin />
        ) : undefined}
        <img
          src={itemImageURL}
          alt={itemName}
          onLoad={imageLoaded}
          style={{ visibility: loading ? "hidden" : "visible" }}
        />
      </figure>
      <div className="ingredient-name" name={itemName}>
        {itemName}
      </div>
      {isSelected ? (
        <IngredientControls
          ingredientRef={itemName}
          substituteIngredientHandler={substituteIngredientHandler}
          deleteIngredientHandler={deleteIngredientHandler}
        />
      ) : undefined}
    </li>
  );
};

export default IngredientItem;
