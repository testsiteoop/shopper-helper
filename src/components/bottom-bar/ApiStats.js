import React, { useState, useEffect } from "react";
import StatusLight from "./StatusLight";
import StatsItem from "./StatsItem";

const ApiStats = ({ quotaUsed, quotaAllowance, loading }) => {
  // API Reset time -> 00:00:00 UTC
  const [time, setTime] = useState(new Date());

  const changeTime = () => {
    const time = new Date();
    const hoursUTC = time.getUTCHours();
    time.setHours(
      23 - hoursUTC,
      59 - time.getMinutes(),
      60 - time.getSeconds() //24 hrs to the second
    );
    setTime(time);
  };

  useEffect(() => {
    const tick = setInterval(() => {
      changeTime();
    }, 1000);
    return () => clearInterval(tick);
  });

  //API points usage
  const usageRatio = quotaUsed / quotaAllowance;

  return (
    <div className="api-stats">
      <StatsItem
        className="load-indicator"
        icon={loading ? "spinner" : "check-circle"}
        spin={loading}
      />
      <StatusLight usageRatio={usageRatio} />
      <StatsItem
        className="api-time"
        text={`Api usage resets in ${time.toLocaleTimeString(undefined, {
          hour12: false,
        })}`}
        icon="clock"
      />
    </div>
  );
};

export default ApiStats;
