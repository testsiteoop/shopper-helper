import React from "react";

const StatusLight = ({ usageRatio }) => {
  const statusColor = (usageRatio) => {
    if (usageRatio > 0.95) {
      return "red";
    } else if (usageRatio > 0.75) {
      return "yellow";
    } else {
      return "green";
    }
  };
  return (
    <span
      className={`status-light ${statusColor(usageRatio)}`}
      title={usageRatio > 0.95 ? "All API points have been used" : "All good"}
    ></span>
  );
};

export default StatusLight;
