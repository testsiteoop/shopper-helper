import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const StatsItem = ({ className, text, icon, spin }) => {
  return (
    <div className={`api-stats-item ${className}`}>
      <FontAwesomeIcon className="icon" icon={icon} spin={spin} />
      {text}
    </div>
  );
};

export default StatsItem;
