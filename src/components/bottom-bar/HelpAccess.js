import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const HelpAccess = ({ modalHelpHandler }) => {
  return (
    <div
      className="help-access"
      data-micromodal-trigger="modal"
      onClick={modalHelpHandler}
    >
      <FontAwesomeIcon className="icon" icon="question-circle" />
    </div>
  );
};

export default HelpAccess;
