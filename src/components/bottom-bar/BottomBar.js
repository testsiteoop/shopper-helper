import React from "react";
import HelpAccess from "./HelpAccess";
import ApiStats from "./ApiStats";

const BottomBar = ({
  quotaUsed,
  quotaAllowance,
  modalHelpHandler,
  loading,
}) => {
  return (
    <footer className="bottom-bar">
      <ApiStats
        quotaUsed={quotaUsed}
        quotaAllowance={quotaAllowance}
        loading={loading}
      />
      <HelpAccess modalHelpHandler={modalHelpHandler} />
    </footer>
  );
};

export default BottomBar;
