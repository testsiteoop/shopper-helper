import React, { useState, useRef } from "react";
import logo from "../../assets/images/shopper-helper-icon.png";
import AsyncCreatableSelect from "react-select/async-creatable";
import { createFilter } from "react-select";
import { debounce } from "throttle-debounce";
import axios from "axios";

const TopBar = ({
  notyf,
  setLoadingState,
  setApiQuotaUsedState,
  addIngredientHandler,
  modalHelpHandler,
  inputMaxLength,
}) => {
  const API_KEY = process.env.REACT_APP_API_KEY,
    SEARCH_INPUT_PLACEHOLDER = "Search for ingredients!",
    API_BASE_URL_AUTOCOMPLETE =
      "https://api.spoonacular.com/food/ingredients/autocomplete/",
    API_AUTOCOMPLETE_MAX_RESULTS = 100;

  const loadOptionsDebounced = useRef(debounce(500, getAutocomplete)).current;
  const selectRef = useRef();

  function getAutocomplete(input, callback) {
    // callback: function handleInputChange(options) from react-select
    setLoadingState(true);
    axios
      .get(API_BASE_URL_AUTOCOMPLETE, {
        params: {
          apiKey: API_KEY,
          query: input,
          number: API_AUTOCOMPLETE_MAX_RESULTS,
          metaInformation: true,
        },
        timeout: 6000,
      })
      .then((response) => {
        const { data } = response;
        if (data.status === "failure") {
          if (data.code === 402) {
            // all api points have been used
            setApiQuotaUsedState(150);
            notyf.error({
              message: "No API points left, try again later!",
              position: { x: "right", y: "top" },
            });
            callback([]); //no options to display in dropdown
            return;
          }
        }
        // necessary to add "label" property to each object for react-select to display/process it properly
        const labeledItems = data.map((item) => {
          return { ...item, label: item.name };
        });
        callback(labeledItems);
      })
      .catch((error) => {
        let errorMessage = "Something unexpected has happened. Sorry!";
        if (error.response.data) {
          const { status, code } = error.response.data;
          if (status === "failure") {
            if (code === 401) {
              errorMessage =
                "Unauthorized. Please check the docs on how to set up an API key.";
            }
          }
        }
        notyf.error({
          message: errorMessage,
          position: { x: "right", y: "top" },
        });
        selectRef.current.blur();
      })
      .finally(() => setLoadingState(false));
  }

  function loadOptions(input, callback) {
    loadOptionsDebounced(input, callback);
  }

  function getNewOptionData(input, optionLabel) {
    const obj = {
      name: input,
      label: optionLabel, // needed to display as option in react-select
      id: undefined,
      __isNew__: true,
    };
    return obj;
  }

  function onChangeHandler(selectedItem, { action }) {
    if (action === "select-option" || action === "create-option") {
      addIngredientHandler(selectedItem);
    }
  }

  function onInputChangeHandler(input, { action }) {
    // Workaround for maxLength property of input
    if (inputMaxLength && input.length > inputMaxLength) {
      return;
    }
    setInputValue(input);
  }

  const [inputValue, setInputValue] = useState("");

  return (
    <header className="top-bar">
      <figure
        className="logo"
        onClick={modalHelpHandler}
        data-micromodal-trigger="modal"
      >
        <img src={logo} alt="logo" />
      </figure>

      <div className="search">
        <AsyncCreatableSelect
          ref={selectRef}
          inputValue={inputValue}
          value={inputValue}
          placeholder={SEARCH_INPUT_PLACEHOLDER}
          aria-label={SEARCH_INPUT_PLACEHOLDER}
          maxMenuHeight={150}
          styles={{
            dropdownIndicator: (base) => ({ ...base, display: "none" }),
            indicatorSeparator: (base) => ({ ...base, display: "none" }),
          }}
          cacheOptions
          loadOptions={loadOptions}
          onChange={onChangeHandler}
          filterOption={createFilter({
            ignoreCase: true,
            ignoreAccents: true,
            trim: true,
          })}
          onInputChange={onInputChangeHandler}
          getNewOptionData={getNewOptionData}
          noOptionsMessage={() => "No ingredients to show"}
          blurInputOnSelect
          autoFocus
        />
      </div>
    </header>
  );
};

export default TopBar;
