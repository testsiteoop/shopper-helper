import React from "react";

import MicroModal from "micromodal";
import axios from "axios";
import { Notyf } from "notyf";
import ReactGA from "react-ga";

import TopBar from "./components/top-bar";
import BottomBar from "./components/bottom-bar";
import IngredientList from "./components/ingredient-list";
import Modal from "./components/modal";
import ModalContentHelp from "./components/modal/ModalContentHelp";
import ModalContentImage from "./components/modal/ModalContentImage";
import ModalContentSubstitute from "./components/modal/ModalContentSubstitute";

import "notyf/notyf.min.css";
import "animate.css";
import "./assets/micromodal.css";
import "./App.scss";

// ICONS
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSearch,
  faQuestionCircle,
  faClock,
  faSpinner,
  faShareAlt,
  faSyncAlt,
  faTimesCircle,
  faArrowCircleUp,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { faBitbucket } from "@fortawesome/free-brands-svg-icons";

library.add(
  faSearch,
  faQuestionCircle,
  faClock,
  faSpinner,
  faShareAlt,
  faSyncAlt,
  faTimesCircle,
  faArrowCircleUp,
  faCheckCircle,
  faBitbucket
);

const APP_NAME = "shopper-helper",
  GOOGLE_ANALYTICS_KEY = process.env.REACT_APP_GOOGLE_ANALYTICS_KEY,
  API_KEY = process.env.REACT_APP_API_KEY,
  API_QUOTA_FREE_PLAN = 150,
  API_IMG_SIZE_SMALL = "100x100",
  API_IMG_SIZE_MEDIUM = "250x250",
  // API_IMG_SIZE_LARGE = "500x500",
  // API_HEADER_QUOTA_USED = "x-api-quota-used",
  API_IMG_PARTIAL_URL_BASE = "https://spoonacular.com/cdn/ingredients_",
  API_IMG_SMALL_URL = `${API_IMG_PARTIAL_URL_BASE}${API_IMG_SIZE_SMALL}/`,
  API_IMG_MEDIUM_URL = `${API_IMG_PARTIAL_URL_BASE}${API_IMG_SIZE_MEDIUM}/`,
  API_BASE_URL_SUBSTITUTE_BY_NAME =
    "https://api.spoonacular.com/food/ingredients/substitutes/",
  API_TIMEOUT_THRESHOLD = 6000,
  APP_NAME_DISPLAY = "Shopper Helper",
  APP_LOCALSTORAGE_PROPERTY_NAME = "ingredient_store",
  APP_INPUT_SEARCH_MAX_LENGTH = 150;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      errorMessage: "",
      loading: false,
      api_quota_allowance: API_QUOTA_FREE_PLAN,
      api_quota_used: 0,
      ingredients: new Map(),
      modal: { title: APP_NAME_DISPLAY, content: <>welcome!</> },
    };

    // toast notifications
    this.notyf = new Notyf({ duration: 2500 });

    // Google Analytics
    ReactGA.initialize(GOOGLE_ANALYTICS_KEY, {
      debug: false,
      testMode: process.env.NODE_ENV === "test",
    });
    ReactGA.set({
      appName: APP_NAME,
    });

    //bindings
    this.selectIngredientHandler = this.selectIngredientHandler.bind(this);
    this.addIngredientHandler = this.addIngredientHandler.bind(this);
    this.substituteIngredientHandler = this.substituteIngredientHandler.bind(
      this
    );
    this.deleteIngredientHandler = this.deleteIngredientHandler.bind(this);

    this.resetModal = this.resetModal.bind(this);
    this.modalHandler = this.modalHandler.bind(this);
    this.modalIngrImageHandler = this.modalIngrImageHandler.bind(this);
    this.modalHelpHandler = this.modalHelpHandler.bind(this);

    this.setLoadingState = this.setLoadingState.bind(this);
    this.setApiQuotaUsedState = this.setApiQuotaUsedState.bind(this);
  }

  // HANDLERS
  selectIngredientHandler(e) {
    e.stopPropagation();
    const selectedName = e.currentTarget.attributes.name.value;
    const ingredients = this.state.ingredients;
    const updated = new Map();
    ingredients.forEach((ingredient, key) => {
      if (key === selectedName) {
        updated.set(key, { ...ingredient, isSelected: true });
      } else {
        updated.set(key, { ...ingredient, isSelected: false });
      }
    });
    this.setState({ ingredients: updated }, this.initModal); //for controls-substitute
  }

  addIngredientHandler(ingredient) {
    if (ingredient === null) {
      return;
    }
    const oldIngredients = this.state.ingredients;
    const { id, name, image, __isNew__ } = ingredient;
    if (oldIngredients.has(name)) {
      this.notyf.success(`${name} is already on the list!`);
      return;
    }
    const ingredients = new Map(oldIngredients);
    ingredients.set(name, {
      name: name,
      image: __isNew__ ? "default.jpg" : image,
      id: id,
      isSelected: false,
    });
    this.setState({ ingredients: ingredients }, () => {
      this.initModal();
      this.notyf.success(`${name} added to the list!`);
      this.updateStorage();
      ReactGA.event({
        category: "User",
        action: "Added an Ingredient",
        label: name,
      });
    });
  }

  substituteIngredientHandler(e) {
    e.stopPropagation();
    const { ingredient } = e.currentTarget.dataset;
    this.setLoadingState(true);
    axios
      .get(API_BASE_URL_SUBSTITUTE_BY_NAME, {
        params: {
          apiKey: API_KEY,
          ingredientName: ingredient,
        },
        timeout: API_TIMEOUT_THRESHOLD,
      })
      .then((response) => {
        const { data } = response;
        if (!data) {
          this.notyf.error("Something's not right... Please try Again!");
          return;
        }
        if (data.status === "failure") {
          if (data.code === 402) {
            // all api points have been used
            this.setApiQuotaUsedState(150);
            this.notyf.error({
              message: "No API points left, try again later!",
            });
          }
          const title = `Oh no!`;
          const content = (
            <p>
              We couldn't find any substitutes for <b>{ingredient}</b>... Sorry!
            </p>
          );
          this.modalHandler(title, content);
          return;
        }
        const title = `You can substitute ${ingredient} for any of these:`;
        const content = (
          <ModalContentSubstitute substitutes={data.substitutes} />
        );
        this.modalHandler(title, content);
      })
      .catch((error) => {
        this.modalHandler(
          "Oh No!",
          <p>
            Something unexpected has happened.
            <br /> Sorry!
          </p>
        );
      })
      .finally(() => {
        this.setLoadingState(false);
        ReactGA.event({
          category: "User",
          action: "Queried for Ingredient Substitutes",
          label: ingredient,
        });
      });
  }

  deleteIngredientHandler(e) {
    // prevents onClick handler from parent and override delete bc of setState
    e.stopPropagation();
    const updated = new Map(this.state.ingredients);
    const name = e.currentTarget.dataset.ingredient;
    updated.delete(name);
    this.setState({ ingredients: new Map(updated) }, () => {
      this.updateStorage();
      ReactGA.event({
        category: "User",
        action: "Deleted an Ingredient",
        label: name,
      });
    });
  }

  // MODAL
  modalHandler(title, content) {
    this.setState({ modal: { title: title, content: content } });
  }

  modalIngrImageHandler(e) {
    e.stopPropagation();
    const ingredient = this.state.ingredients.get(
      e.currentTarget.attributes.name.value
    );
    this.modalHandler(
      ingredient.name,
      <ModalContentImage
        alt={ingredient.name}
        itemImageURL={`${API_IMG_MEDIUM_URL}${ingredient.image}`}
      />
    );
    ReactGA.event({
      category: "User",
      action: "Looked Ingredient Image",
      label: ingredient.name,
    });
  }

  modalHelpHandler(e) {
    e.stopPropagation();
    this.modalHandler(APP_NAME_DISPLAY, <ModalContentHelp />);
    ReactGA.event({
      category: "User",
      action: "Opened App Help/About",
      label: APP_NAME_DISPLAY,
    });
  }

  // UTILS
  updateStorage() {
    localStorage.setItem(
      APP_LOCALSTORAGE_PROPERTY_NAME,
      JSON.stringify([...this.state.ingredients])
    );
  }

  loadIngredientsFromStorage() {
    if (localStorage.getItem(APP_LOCALSTORAGE_PROPERTY_NAME)) {
      const storedIngredients = localStorage.getItem(
        APP_LOCALSTORAGE_PROPERTY_NAME
      );
      const ingredients = new Map(JSON.parse(storedIngredients));
      // No selected ingredients at startup
      ingredients.forEach((ingredient, key) => {
        ingredients.set(key, { ...ingredient, isSelected: false });
      });
      this.setState({ ingredients: ingredients }, this.initModal);
    }
  }

  setLoadingState(boolState) {
    this.setState({ loading: boolState });
  }

  setApiQuotaUsedState(points) {
    this.setState({ api_quota_used: points });
  }

  initModal() {
    MicroModal.init({
      awaitCloseAnimation: true,
      onClose: this.resetModal,
    });
  }

  resetModal() {
    setTimeout(() => {
      this.setState({
        modal: { title: APP_NAME_DISPLAY, content: <>welcome!</> },
      });
    }, 300);
  }

  // LIFECYCLE
  componentDidMount() {
    this.loadIngredientsFromStorage();
    ReactGA.pageview(window.location.pathname + window.location.search);
    ReactGA.event({
      category: "App",
      action: "Application loaded",
      label: APP_NAME,
      nonInteraction: true,
    });
  }

  render() {
    const { api_quota_allowance, api_quota_used, modal } = this.state;
    return (
      <div className={`app container ${APP_NAME}`}>
        <TopBar
          notyf={this.notyf}
          setLoadingState={this.setLoadingState}
          setApiQuotaUsedState={this.setApiQuotaUsedState}
          addIngredientHandler={this.addIngredientHandler}
          modalHelpHandler={this.modalHelpHandler}
          inputMaxLength={APP_INPUT_SEARCH_MAX_LENGTH}
        />
        <IngredientList
          ingredients={this.state.ingredients}
          modalIngrImageHandler={this.modalIngrImageHandler}
          selectIngredientHandler={this.selectIngredientHandler}
          substituteIngredientHandler={this.substituteIngredientHandler}
          deleteIngredientHandler={this.deleteIngredientHandler}
          baseImageUrl={API_IMG_SMALL_URL}
        />
        <BottomBar
          quotaUsed={api_quota_used}
          quotaAllowance={api_quota_allowance}
          modalHelpHandler={this.modalHelpHandler}
          loading={this.state.loading}
        />
        <Modal
          id="modal"
          title={modal.title}
          content={modal.content}
          loading={this.state.loading}
        />
      </div>
    );
  }
}

export default App;
