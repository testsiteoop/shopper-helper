![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/testsiteoop/shopper-helper/master?logo=Bitbucket&logoColor=%230052CC&style=flat-square)

# Shopper Helper

![Shopper Helper Screenshot](src/assets/images/shopper-helper-screenshot-readme.png)
  
Shopper Helper keeps a **list of ingredients** handy while you go grocery shopping.  
If you find one of them isn't available, the "**substitute**" button will suggest you other options!

See it live @ [demo.bloq.cl/shopper-helper/](https://demo.bloq.cl/shopper-helper/)

## Technology

Simple SPA (_Single Page Application_) in **JS** using **React**, which talks to the **Spoonocular Api** in the background.  
**Bitbucket Pipelines** provide **CI/CD** capabilites building the project and deploying it to an **AWS S3 Bucket**.

### Built With

- ReactJS
- Spoonocular API
- AWS S3
- Bitbucket Pipelines (CI/CD)

### Libraries

- Axios
- Fontawesome
- Animate CSS
- React-Select
- Micromodal
- Throttle-debounce
- Node Sass

## Local Installation

If you want to try it locally:

1. Install Node
2. Clone the repo or download
3. Get a new [Spoonocular API key](https://spoonacular.com/food-api/console#Dashboard)
4. in the `./src` folder create a `.env.local` file with the following line: `REACT_APP_API_KEY=your_api_key`
5. `npm install`
6. `npm start`

If you modify the `.env.local` file, remember to restart the developement server for the changes to take effect.

## Authors

- **Andrés Cruz García** - _Check out the rest!_ - [demo.bloq.cl](https://demo.bloq.cl)

### Contact

[![Linkedin](https://img.shields.io/badge/LinkedIn-blue?style=flat-square&logo=linkedin&labelColor=blue)](https://www.linkedin.com/in/acruzg/?locale=en_US)

## Acknowledgments

- Grocery Bag icon made by [Smashicons](https://www.flaticon.com/authors/smashicons "Smashicons") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon")
